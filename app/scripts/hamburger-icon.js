/*LENGUAJE JAVA SCRIPT*/

/*Funciones de la página que esperan que haya una acción en la página, ejm un click*/
console.log('Cargando Hamburger Icon...');
(function () {/*Funciones pricipal*/
    const MAIN_OBJ = {/*declaración del objeto*/
        init: function () {
            this.eventHandlers() /*Acciones en la página, eventos*/
        },
        /*eventHandlers activa los eventos en la página*/
        eventHandlers: function (){
            document.querySelector('.hamburger-icon').addEventListener('click', function(){
                document.querySelector('.menu-container').classList.toggle('menu-open');
            });
        /*Los clicks me cambian las clases: hamburger, menu, etc*/
        document.querySelector('.sub-hamburger-icon').addEventListener('click', function(){
            document.querySelector('.sub-menu-container').classList.toggle('sub-menu-open');
        });
        }
    }
    MAIN_OBJ.init();
})();