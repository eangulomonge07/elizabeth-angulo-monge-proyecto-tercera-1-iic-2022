/*LENGUAJE JAVA SCRIPT*/

console.log('Cargando Accordion Icon...');
    const dataAccordion = [ //Creación de tabs y acordiones
        {
            "title": "Gran variedad en Laptops",
            "desc": "Laptops para todo Costa Rica. Además de laptops, le ofrecemos todos los componentes que su portátil necesita."
        },
        {
            "title": "Tablets para todos los gustos",
            "desc": "Las tablets están orientadas a la navegación web y manipulación multimedia. Ofrecemos tablets de las marcas Samsung, Logic, AOC y más."
        },
        {
            "title": "Consolas de última generación",
            "desc": "Ofrecemos consolas y videojuegos para desafiar los límites de su entretenimiento. Consolas Xbox, PlayStation y Nintendo. Además, tenemos accesorios para el mayor disfrute de los juegos."
        },
    ];

    (function () {
        let ACCORDION = {/*declaración de la acción o variable*/
            init: function () {
                let _self = this
                //Llamamos las funciones:
                this.insertData(_self);
                this.eventHandler(_self);
            },

            insertData: function (_self) {
                dataAccordion.map(function (item, index) {
                    document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend',
                    _self.tplAccordionItem(item));
                });          
            },

            eventHandler: function (_self) {
                let arrayRefs = document.querySelectorAll('.accordion-title');
                for (let x = 0; x < arrayRefs.length; x++) { //x++ hacia arriba de 1 en adelante
                    arrayRefs[x].addEventListener('click', function (event) {
                        console.log('event', event);
                        _self.showTab(event.target);
                    });                
                }          
            },

            tplAccordionItem: function (item) {
                return (`<div class='accordion-item'>
                <div class='accordion-title'><p>${item.title}</p></div>
                <div class='accordion-desc'><p>${item.desc}</p></div>
                </div>`) //Estas comillas sirven para llamar a otro lenguaje, en este caso html

            },

            showTab: function (refItem) {
            let activeTab = document.querySelector('.tab-active')
            if (activeTab) { //Si el tab está activo
                activeTab.classList.remove('tab-active');
            } 
            console.log('show tab', refItem);
            refItem.parentElement.classList.toggle('tab-active');
        },


        }
        ACCORDION.init();
    })();